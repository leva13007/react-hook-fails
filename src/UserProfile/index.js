import React, {useEffect, useState} from "react";

export const UserProfile = () => {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');

  const fullName = `${firstName} ${lastName}`;

  const submitFormHandler = (e) => {
    e.preventDefault();
    console.log('submitFormHandler', {})
  }

  return (
    <div className="container">
      <div className="row">
        <h1>User profile</h1>
      </div>
      <div className="row">
        <form className="card col p-3"  onSubmit={submitFormHandler}>
          <div className="mb-3">
            <input className="form-control" type="text" placeholder="Type first name here"
                  value={firstName} onChange={(e) => setFirstName(e.target.value)}/>
          </div>
          <div className="mb-3">
            <input className="form-control" type="text" placeholder="Type last name here"
                   value={lastName} onChange={(e) => setLastName(e.target.value)}/>
          </div>
          <div className="mb-3">
            <h3>Full name: <i>{fullName}</i></h3>
          </div>
        </form>
      </div>
    </div>
  )
}