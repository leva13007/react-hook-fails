import React, {useState} from "react";

export const Counter = () => {
  const [count, setCount] = useState(0);
  
  const counterHandler = (value) => {
    setCount(prevState => {
      const temp = prevState + value;
      console.log('count 1',temp);
      return temp;
    });
    setCount(prevState => {
      const temp = prevState + value;
      console.log('count 2',temp);
      return temp;
    });
  }
  
  return (
    <div className="container">
      <div className="row">
        <div className="card p-3 m-3">
          <h1 className="col-12">Counter</h1>
          <div className="col-12">
            Counter value: {count}
          </div>
          <div className="col-12">
            <button className="btn btn-primary me-1" onClick={() => counterHandler(-1)}>Decrement</button>
            <button className="btn btn-danger" onClick={() => counterHandler(1)}>Increment</button>
          </div>
        </div>
      </div>
    </div>
  )
}