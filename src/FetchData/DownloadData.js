import React, {useEffect} from "react";
import {useFetch} from "./useFetch";

export const DownloadData = ({url}) => {
  const {loading,error,data} = useFetch(url);

  useEffect(() => {
    console.log("data",data)
  },[data])

  return (
    <div className="card col-12 p-3">
      {
        loading ? (
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        ) : (
          <h3>
            {url}
          </h3>
        )
      }
    </div>
  )
}