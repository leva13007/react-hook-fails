import React, {useState} from "react";
import {DownloadData} from "./DownloadData";

export const FetchData = () => {
  const [isShowData, setShownStatus] = useState(true);
  const [url, setUrl] = useState('google.com'); // just const

  return (
    <div className="container">
      <div className="row p-3">
        {
          isShowData && (
            <DownloadData url={url} />
          )
        }
        <div className="card col-12 p-3">
          <button onClick={() => setShownStatus(false)} className="btn btn-danger">Hide downloaded content</button>
        </div>
        <div className="card col-12 p-3">
          <button onClick={() => setUrl('amazon.com')} className="btn btn-success">Get amazon</button>
        </div>
        <div className="card col-12 p-3">
          <button onClick={() => setUrl('ebay.com')} className="btn btn-info">Get ebay</button>
        </div>
      </div>
    </div>
  )
}