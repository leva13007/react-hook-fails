import {useEffect, useState} from "react";

export const useFetch = (url) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data, setData] = useState(null);

  useEffect(() => {
    const controller = new AbortController()
    setLoading(true);
    fetch(url, {signal: controller.signal})
      .then(res => setData(res))
      .catch(err => setError(err))
      .finally(() => setLoading(false));

    return () => controller.abort();
  }, [url]);

  return {loading,error,data}
}