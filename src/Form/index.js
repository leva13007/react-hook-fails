import {useRef, useState} from "react";

function Form() {
  console.log("Render Form");
  const nameRef = useRef(null);
  const emailRef = useRef(null);

  const submitFormHandler = (e) => {
    e.preventDefault();
    console.log('submitFormHandler', {
      name: nameRef.current.value,
      email: emailRef.current.value,
    })
  }

  return (
    <div className="container">
      <div className="row">
        <h1>Form</h1>
      </div>
      <div className="row">
        <form className="card col p-3"  onSubmit={submitFormHandler}>
          <div className="mb-3">
            <input className="form-control" type="text" ref={nameRef} placeholder="Type name here"/>
          </div>
          <div className="mb-3">
            <input className="form-control" type="text" ref={emailRef} placeholder="Type email here"/>
          </div>
          <div className="">
            <button className="btn btn-primary" type="submit">Submit</button>
          </div>

        </form>
      </div>
    </div>
  );
}

export default Form;
